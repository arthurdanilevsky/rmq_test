# pull containers(if necessary) and start project
.PHONY: up
up:
	docker-compose up -d rabbitmq
	docker-compose up -d mongo
	go run main.go

# stop all containers
.PHONY: stop
stop:
	docker-compose stop