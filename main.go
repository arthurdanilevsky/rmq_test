package main

import (
	"context"
	"flag"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"rmq_test/app"
	"rmq_test/conf"
)

func main() {
	path := flag.String("path", "./conf/config.json", "Path to config file")
	flag.Parse()
	config := conf.NewFromFile(*path)
	client := ConnectDB(config.Mongo.URI())
	application := app.NewApp(client, config)
	if err := application.Init(); err != nil {
		logrus.WithError(err).Panic("Can't init an application")
	}
	application.Run()
}

func ConnectDB(uri string) *mongo.Client {
	// Create client
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		logrus.WithError(err).Panic("Can't create db client")
	}
	// Create connection
	if err := client.Connect(context.TODO()); err != nil {
		logrus.WithError(err).Panic("Can't create connection")
	}
	// Check the connection
	if err := client.Ping(context.TODO(), nil); err != nil {
		logrus.WithError(err).Panic("Can't ping db")
	}
	return client
}
