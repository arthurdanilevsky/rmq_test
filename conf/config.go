package conf

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/sirupsen/logrus"
)

// Main is the whole application configuration
type Main struct {
	RabbitMQ  RabbitMQConfig `json:"rabbit_mq"`
	QueueName string         `json:"queue_name"`
	Mongo     Mongo          `json:"mongo"`
}

// RabbitMQConfig stores connection settings
type RabbitMQConfig struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	VHost    string `json:"vhost"`
}

// URI returns string URI for connection to RMQ
func (rmq RabbitMQConfig) URI() string {
	return fmt.Sprintf("amqp://%s:%s@%s:%d%s",
		rmq.User,
		rmq.Password,
		rmq.Host,
		rmq.Port,
		rmq.VHost,
	)
}

// Mongo stores connection settings
type Mongo struct {
	Host string `json:"host"`
	Port int    `json:"port"`
}

// URI returns string URI for connection to mongoDB
func (m Mongo) URI() string {
	return fmt.Sprintf("mongodb://%s:%d",
		m.Host,
		m.Port,
	)
}

// NewFromFile creates new conf from selected file
func NewFromFile(path string) *Main {
	configJSON, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.WithError(err).WithField("path to file", path).Panic("Can't read config file")
	}
	var c Main
	if err := json.Unmarshal(configJSON, &c); err != nil {
		logrus.WithError(err).WithField("path to file", path).Panic("Can't unmarshal config")
	}
	return &c
}
