### Running the Application

1. Ensure `make` is installed on your system
2. Install Docker and Docker Compose

Clone the application and run it:

    git clone git@bitbucket.org:arthurdanilevsky/rmq_test.git
    cd ./rmq_test
    make up

Use `Ctrl + C` and `make stop` for closing application and stopping containers.