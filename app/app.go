package app

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"rmq_test/conf"
)

const (
	AlarmStatusOngoing  = "ONGOING"
	AlarmStatusResolved = "RESOLVED"
)

// App is application abstract
type App struct {
	dbClient     *mongo.Client
	config       *conf.Main
	connection   *amqp.Connection
	closeErrorCh chan *amqp.Error
	rabbitCh     *amqp.Channel
	queue        amqp.Queue
	msgCh        <-chan amqp.Delivery
}

// NewApp returns an App
func NewApp(dbClient *mongo.Client, config *conf.Main) *App {
	return &App{
		dbClient: dbClient,
		config:   config,
	}
}

// DialRMQ tries to connect to the RabbitMQ server as long as it takes to establish a connection
func (a *App) DialRMQ() *amqp.Connection {
	for {
		conn, err := amqp.Dial(a.config.RabbitMQ.URI())
		if err != nil {
			logrus.WithError(err).Errorf("Trying to connect to RabbitMQ at %s\n", a.config.RabbitMQ.URI())
			time.Sleep(time.Second * 1)
			continue
		}
		logrus.Info("Connected")
		return conn
	}
}

// Run read and handle messages from queue, keep connection to RMQ
func (a *App) Run() {
	for {
		select {
		case closeErr := <-a.closeErrorCh:
			logrus.WithError(closeErr).Error("Connection to RMQ was broken")
			// reconnection
			logrus.Info("Reconnecting to RMQ")
			if err := a.Init(); err != nil {
				logrus.WithError(err).Panic("Can't init an application")
			}

		case msg, ok := <-a.msgCh:
			logrus.Debugf("Got from msg channel\na.msgCh = %+v\nmsg = %+v\nisOpen = %v\n", a.msgCh, msg, ok)
			if !ok {
				logrus.Info("Message channel was closed, recreate it")
				a.Consume()
				// no need to handle an empty nil message from closed channel
				continue
			}
			if err := a.HandleMsg(msg.Body); err == nil {
				msg.Ack(false)
			}
		}
	}
}

// Alarm is an abstraction for storing alarms in db
type Alarm struct {
	Component string `bson:"component"`
	Resource  string `bson:"resource"`
	Crit      int    `bson:"crit"`
	LastMsg   string `bson:"last_msg"`
	FirstMsg  string `bson:"first_msg"`
	StartTime int    `bson:"start_time"`
	LastTime  int    `bson:"last_time"`
	Status    string `bson:"status"`
}

// Event is an event abstraction
type Event struct {
	Source    string `json:"source"` // Unused field?
	Component string `json:"component"`
	Resource  string `json:"resource"`
	Crit      int    `json:"crit"`
	Message   string `json:"message"`
	Timestamp int    `json:"timestamp"`
}

// HandleMsg creates new or updates an existing alarm for an event from queue
func (a *App) HandleMsg(msg []byte) error {
	var event Event
	if err := json.Unmarshal(msg, &event); err != nil {
		logrus.WithField("message", string(msg)).WithError(err).
			Error("Can't unmarshal message body from RabbitMQ")
		return err
	}
	// TODO: validate event fields

	collection := a.dbClient.Database("test").Collection("test_technique")
	filter := bson.M{
		"component": event.Component,
		"resource":  event.Resource,
		"status":    AlarmStatusOngoing,
	}
	var exAlarm Alarm
	err := collection.FindOne(context.TODO(), filter).Decode(&exAlarm)
	if err != nil && err != mongo.ErrNoDocuments {
		logrus.WithField("filter", fmt.Sprintf("%+v", filter)).
			WithField("event", fmt.Sprintf("%+v", event)).
			WithError(err).Error("Can't find an alarm in db")
		return err
	}

	if err == mongo.ErrNoDocuments {
		if event.Crit == 0 {
			return nil
		}
		// insert new and return
		// TODO: prevent injection
		newAlarm := Alarm{
			Component: event.Component,
			Resource:  event.Resource,
			Crit:      event.Crit,
			LastMsg:   event.Message,
			FirstMsg:  event.Message,
			StartTime: event.Timestamp,
			LastTime:  event.Timestamp,
			Status:    AlarmStatusOngoing,
		}
		if _, err := collection.InsertOne(context.TODO(), newAlarm); err != nil {
			logrus.WithField("newAlarm", fmt.Sprintf("%+v", newAlarm)).
				WithField("event", fmt.Sprintf("%+v", event)).
				WithError(err).
				Error("Can't insert an alarm in db")
			return err
		}
		return nil
	}

	// update existing
	// TODO: prevent injection
	fields := map[string]interface{}{
		"crit":      event.Crit,
		"last_msg":  event.Message,
		"last_time": event.Timestamp,
	}
	if event.Crit == 0 {
		fields["status"] = AlarmStatusResolved
	}
	if _, err := collection.UpdateOne(context.TODO(), filter, bson.M{"$set": fields}); err != nil {
		logrus.WithField("exAlarm", fmt.Sprintf("%+v", exAlarm)).
			WithField("event", fmt.Sprintf("%+v", event)).
			WithError(err).
			Error("Can't update an alarm in db")
		return err
	}

	return nil
}

// QueueDeclare declares Exchange, Queue and Binding
func (a *App) QueueDeclare(name string) (amqp.Queue, error) {
	if err := a.rabbitCh.ExchangeDeclare(
		name,     // name
		"direct", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // noWait
		nil,      // arguments
	); err != nil {
		logrus.WithError(err).WithField("queue name", name).Error("Can't declare an exchange")
		return amqp.Queue{}, err
	}

	queue, err := a.rabbitCh.QueueDeclare(
		name,  //name
		true,  //durable
		false, //autoDelete
		false, //exclusive
		false, //noWait
		nil,   //args
	)
	if err != nil {
		logrus.WithError(err).WithField("queue name", name).Error("Can't declare a queue")
		return amqp.Queue{}, err
	}

	if err := a.rabbitCh.QueueBind(
		name,  // name of the queue
		name,  // bindingKey
		name,  // sourceExchange
		false, // noWait
		nil,   // arguments
	); err != nil {
		logrus.WithError(err).WithField("queue name", name).Error("Can't bind queue to exchange")
		return amqp.Queue{}, err
	}

	return queue, nil
}

// Init is initialization of Connection, Queue and Consume channel
func (a *App) Init() error {
	logrus.Info("Init RMQ")
	var err error

	// Create connection
	a.connection = a.DialRMQ()
	a.closeErrorCh = make(chan *amqp.Error)
	a.connection.NotifyClose(a.closeErrorCh)

	// Create channel
	a.rabbitCh, err = a.connection.Channel()
	if err != nil {
		logrus.WithError(err).Error("Can't create RMQ Channel")
		return err
	}

	// Set prefetch count
	if err := a.rabbitCh.Qos(
		2,     // prefetch count
		0,     // prefetch size
		false, // global
	); err != nil {
		logrus.WithError(err).Error("Can't set prefetch count")
		return err
	}

	if err := a.Consume(); err != nil {
		return err
	}

	return nil
}

// Consume starts reading messages from rabbitMQ queue
func (a *App) Consume() error {
	var err error
	//  Declare and bind queue
	if a.queue, err = a.QueueDeclare(a.config.QueueName); err != nil {
		return err
	}

	// Create consumer
	a.msgCh, err = a.rabbitCh.Consume(
		a.config.QueueName,
		a.config.QueueName, // consumerTag,
		false,              // autoAck
		false,              // exclusive
		false,              // noLocal
		false,              // noWait
		nil,                // arguments
	)
	if err != nil {
		logrus.WithError(err).WithField("queue name", a.config.QueueName).Error("Can't create RMQ consumer")
		return err
	}

	return nil
}
